import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

class Author {
  value: string;
  matchLevel: string;
  matchedWords: string[];
}

class CommentText {
  value: string;
  matchLevel: string;
  matchedWords: string[];
}

class StoryTitle {
  value: string;
  matchLevel: string;
  matchedWords: string[];
}

class StoryUrl {
  value: string;
  matchLevel: string;
  matchedWords: string[];
}

class HighlightResult {
  author: Author;
  commentText: CommentText;
  storyTitle: StoryTitle;
  storyUrl: StoryUrl;
}

@Schema()
export class Post extends Document {
  @Prop()
  created_at: Date;
  @Prop()
  title: string;
  @Prop()
  url: string;
  @Prop()
  author: string;
  @Prop()
  points: string;
  @Prop()
  story_text: string;
  @Prop()
  comment_text: string;
  @Prop()
  num_comments: string;
  @Prop()
  story_id: number;
  @Prop()
  story_title: string;
  @Prop()
  story_url: string;
  @Prop()
  parent_id: string;
  @Prop()
  created_at_i: Date;
  @Prop()
  _tags: string[];
  @Prop()
  objectID: string;
  @Prop()
  _highlightResult: HighlightResult;
  @Prop()
  isDeleted: boolean;
}

export const PostSchema = SchemaFactory.createForClass(Post);
