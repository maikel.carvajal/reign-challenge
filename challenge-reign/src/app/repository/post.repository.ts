import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Post } from '../model/post.model';

@Injectable()
export class PostRepository {
  constructor(
    @InjectModel(Post.name)
    private PostModel: Model<Post>,
  ) {}

  async createPost(post: Post): Promise<Post> {
    return this.PostModel.create(post);
  }

  async getPost(id: string): Promise<Post> {
    return this.PostModel.findOne({ objectID: id });
  }

  async getAllPost(): Promise<Post[]> {
    return this.PostModel.find().sort({ created_at: -1 });
  }

  async getPostById(id: string): Promise<Post> {
    return this.PostModel.findById(id);
  }
  async updateIsDeleted(id: string, body: { delete: boolean }): Promise<Post> {
    return this.PostModel.findOneAndUpdate(
      { _id: id },
      {
        isDeleted: body.delete,
      },
      { new: true },
    );
  }
}
