import { Body, Controller, Get, Param, Patch } from '@nestjs/common';
import { PostRepository } from '../repository/post.repository';

@Controller('post')
export class PostController {
  constructor(private readonly postRepository: PostRepository) {}

  @Get('/all')
  async getAllPost() {
    return this.postRepository.getAllPost();
  }

  @Get('/:id')
  async getPostById(@Param('id') id: string) {
    return this.postRepository.getPostById(id);
  }

  @Patch('/delete/:id')
  async updateIsDeleted(
    @Param('id') id: string,
    @Body() body: { delete: boolean },
  ) {
    return this.postRepository.updateIsDeleted(id, body);
  }
}
