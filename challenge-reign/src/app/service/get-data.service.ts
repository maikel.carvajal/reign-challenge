import { HttpService, Injectable, OnModuleInit } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PostRepository } from '../repository/post.repository';

@Injectable()
export class GetDataService implements OnModuleInit {
  constructor(
    private http: HttpService,
    private readonly postRepository: PostRepository,
  ) {}

  @Cron(CronExpression.EVERY_HOUR)
  async getDataFromApi(): Promise<any> {
    const result = await this.http
      .get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();
    await result.data.hits.reduce(async (promise, key) => {
      await promise;
      const post = await this.postRepository.getPost(key.objectID);
      if (post === null) {
        await this.postRepository.createPost(key);
      }
    }, Promise.resolve());
  }

  async onModuleInit() {
    await this.getDataFromApi();
  }
}
