interface Author {
  value: string;
  matchLevel: string;
  matchedWords: string[];
}

interface CommentText {
  value: string;
  matchLevel: string;
  matchedWords: string[];
}

interface StoryTitle {
  value: string;
  matchLevel: string;
  matchedWords: string[];
}

interface StoryUrl {
  value: string;
  matchLevel: string;
  matchedWords: string[];
}

interface HighlightResult {
  author: Author;
  commentText: CommentText;
  storyTitle: StoryTitle;
  storyUrl: StoryUrl;
}

export interface Post{
  _id: string;
  created_at: Date;
  title: string;
  url: string;
  author: string;
  points: string;
  story_text: string;
  comment_text: string;
  num_comments: string;
  story_id: number;
  story_title: string;
  story_url: string;
  parent_id: string;
  created_at_i: Date;
  _tags: string[];
  objectID: string;
  _highlightResult: HighlightResult;
  isDeleted: boolean;
}
