import { Component, OnInit } from '@angular/core';
import {PostService} from '../../services/post.service';
import {Post} from '../../interfaces/Post';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  posts: Post[] = [];
  constructor(
    private postService: PostService,
  ) {
  }

  async ngOnInit(): Promise<void> {
    await this.getPosts();
  }

  async getPosts(): Promise<void> {
    const result = await this.postService.getPosts();
    this.posts = result.filter(p => !!p.title && !p.isDeleted || !!p.story_title && !p.isDeleted);
  }

  async deleteRow(post: Post): Promise<void>{
    await this.postService.updateIsDelete(post._id, {delete: true});
    const index = this.posts.indexOf(post);
    this.posts.splice(index, 1);
  }

}
