import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Post} from '../interfaces/Post';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  URL_BACKEND = `http://localhost:3000`;

  constructor(
    private http: HttpClient
  ) { }

  async getPosts(): Promise<Post[]> {
    return this.http.get<Post[]>(`${this.URL_BACKEND}/post/all`).toPromise();
  }

  async updateIsDelete(id: string, idDelete: { delete: true }): Promise<Post[]> {
    return this.http.patch<Post[]>(`${this.URL_BACKEND}/post/delete/${id}`, idDelete).toPromise();
  }
}
