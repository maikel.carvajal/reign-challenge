import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'datePipe'
})
export class DatePipePipe implements PipeTransform {

  transform(value: Date, ...args: unknown[]): unknown {
    const date = new Date(value);
    const currentDate = moment().startOf('day');
    const start = moment(moment(date).format('L'));
    const end = moment(moment(currentDate).format('L'));
    if (moment(date).format('L') >= moment(currentDate).format('L')) {
      return moment(date).format('LT');
    } else if (end.diff(start, 'days') === 1) {
      return 'Yesterday';
    } else {
      return moment(date).format('MMM Do');
    }
  }

}
