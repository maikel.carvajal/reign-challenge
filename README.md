# Reign Challenge

_This project is a test to enter the REIGN company and I hope it can meet your expectations, my name is Maikel Carvajal Ortiz, I am 24 years old and I am a Computer Engineer from the Universidad Catolica del Norte._

## Starting 🚀

_These instructions will allow you to install / configure the project on your system._

### Installation 🔧

_First of all, we must make sure if we have all the dependencies so that the project works without problems._

**_Install NodeJS_**

1.- NodeJS 14.15.4 LTS (Last version active to date).

**_Clone the project_**

2.- Clone the project on your computer.

## Run the project with Docker image📦
_The project comes configured with its dockerfile files so that it can work without any problem on any operating system._

To initialize the program, you must execute the command `docker-compose up` in the root folder of the repository, where the file` docker-compose.yml` is located, this will start by itself the system, both the backend and the frontend.

**_Client view_**
To access the client view you must go to the url: 
```
http://localhost:4200/
```

**_You do not need anything else_**

Every time the backend is executed, it automatically starts a method that loads the data delivered from the API HN and then every hour the system automatically updates the data from the API HN to the database of this system.


## Run the project manually📦

**_Configure MongoDB_**

1.- Configure a database in MongoDB and put the MONGO URI in an .env file so that the system backend can run without any problems (I left an .env.example in the main folder of the backend giving you an example of how to configure your database of data).

**_Install npm_**

2.- Once the project is cloned and the database configured, we will make sure to install npm in each folder, as well as the server (challenge-reign) and the client (frontend) to run the project without problems. First, you need to access each of these folders via the command line and run the npm install command.


3.- To run the project and make it work, you just have to go to the backend folder (challenge-reign) through the command line and execute the command 'npm run start', then on the command line to the frontend folder (frontend) and run the same command 'npm run start'.

**_You do not need anything else_**
4.- Every time the backend is executed, it automatically starts a method that loads the data delivered from the API HN and then every hour the system automatically updates the data from the API HN to the database of this system.

**_Client view_**

To access the client view you must go to the url: 
```
http://localhost:4200/ 
```


## Built with 🛠️

* [NestJS](https://docs.nestjs.com/) - Framework Backend
* [AngularJS](https://angular.io/docs) - Framework Frontend
* [Docker](https://hub.docker.com/) - Docker

## Author ✒️


* **Maikel Carvajal Ortiz** -  [mkelcarvajal](https://github.com/mkelcarvajal)

---
⌨️ Enjoy!😊
